// eslint-disable-next-line func-names
module.exports = function (config) {
  config.set({
    framework: ['mocha', 'webpack'],
    plugins: ['karma-webpack', 'karma-mocha'],
    files: [
      { pattern: '*_test.js', watched: false },
      { pattern: '**/_test.js', watched: false },
    ],
    preprocessors: {
      '*_test.js': ['webpack'],
      '**/*_test.js': ['webpack'],
    },
    webpack: {
      // custom webpack config
    },
  });
};
