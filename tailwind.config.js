/* eslint-disable global-require */
// eslint-disable-next-line no-unused-vars
const plugin = require('tailwindcss/plugin');

module.exports = {
  presets: [require('@boseverino/design-tokens')],
  content: ['./src/**/*.{html,js,ejs}'],
  plugins: [
    require('./src/base/typography'),
    require('./src/assets/icon'),
    require('./src/components/button/button'),
  ],
};
