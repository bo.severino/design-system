// eslint-disable-next-line no-unused-vars
const tailwindcss = require('tailwindcss');

module.exports = {
  plugins: {
    'postcss-import': {},
    'postcss-preset-env': {},
    tailwindcss: {},
    autoprefixer: {},
    'postcss-nested': {},
  },
};
