const plugin = require('tailwindcss/plugin');

module.exports = plugin.withOptions(() => ({ addBase, theme }) => {
  addBase({
    '.ds-heading': {
      'h1&,h2&,h3&,.h1&,.h2&,.h3&': {
        fontFamily: theme('fontFamily.base'),
        fontWeight: theme('fontWeight.semibold'),
        color: theme('colors.neutral.darker'),
        lineHeight: theme('lineHeight.tiny'),
        fontVariantNumeric: 'lining-nums',
      },
      'h1&, .h1&': {
        fontSize: theme('fontSize.md'),
        '@screen portrait-tablet': {
          fontSize: theme('fontSize.lg'),
        },
      },
      'h2&, .h2&': {
        fontSize: theme('fontSize.sm'),
        '@screen portrait-tablet': {
          fontSize: theme('fontSize.md'),
        },
      },
      'h3&, .h3&': {
        fontSize: theme('fontSize.xs'),
        fontWeight: theme('fontWeight.bold'),
        '@screen portrait-tablet': {
          fontSize: theme('fontSize.sm'),
        },
      },
    },
    '.ds-body': {
      fontFamily: theme('fontFamily.base'),
      fontWeight: theme('fontWeight.regular'),
      color: theme('colors.neutral.dark'),
      lineHeight: theme('lineHeight.distant'),
      fontSize: theme('fontSize.xs'),
      '&.highlight, strong&, b&': {
        fontWeight: theme('fontWeight.semibold'),
      },
    },
    '.ds-caption': {
      fontFamily: theme('fontFamily.base'),
      fontWeight: theme('fontWeight.regular'),
      color: theme('colors.neutral.dark'),
      lineHeight: theme('lineHeight.default'),
      fontSize: theme('fontSize.xxs'),
      '&-150, &.distant': {
        fontWeight: theme('fontWeight.distant'),
      },
      '&.highlight, strong&, b&': {
        fontWeight: theme('fontWeight.semibold'),
      },
    },
  });
});
