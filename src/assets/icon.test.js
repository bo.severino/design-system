const postcss = require('postcss');
const tailwindcss = require('tailwindcss');
const cssMatcher = require('jest-matcher-css');

// eslint-disable-next-line no-unused-vars
const plugin = require('./icon');

expect.extend({
  toMatchCss: cssMatcher,
});

const generatePluginCss = () =>
  postcss(tailwindcss())
    .process('@tailwind components;', {
      // eslint-disable-next-line no-undefined
      from: undefined,
    })
    .then((result) => result.css);

test('it generates the list reset class with variants', () => {
  generatePluginCss().then((css) => {
    expect(css).toMatchCss(`
        .material-symbols-outlined: {
            position: 'relative',
            background-repeat: 'no-repeat',
        }
    `);
  });
});
