const plugin = require('tailwindcss/plugin');

module.exports = plugin.withOptions(() => ({ addUtilities, theme }) => {
  addUtilities({
    '.material-symbols-outlined': {
      '&.sm': {
        width: theme('spacing.16'),
        height: theme('spacing.16'),
        fontSize: theme('spacing.16'),
        lineHeight: theme('spacing.16'),
      },
      '&, &.md': {
        width: theme('spacing.24'),
        height: theme('spacing.24'),
        fontSize: theme('spacing.24'),
        lineHeight: theme('spacing.24'),
      },
      '&.lg': {
        width: theme('spacing.32'),
        height: theme('spacing.32'),
        fontSize: theme('spacing.32'),
        lineHeight: theme('spacing.32'),
      },
      '&, &.outline': {
        outlineStyle: 'none',
        fontVariationSettings: `"FILL" 0`,
      },
      '&.filled': {
        fontVariationSettings: `"FILL" 1`,
      },
    },
  });
});
