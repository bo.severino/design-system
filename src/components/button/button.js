const plugin = require('tailwindcss/plugin');

module.exports = plugin.withOptions(() => ({ addComponents, theme }) => {
  addComponents({
    [`.ds-button`]: {
      '&, &:active, &:focus, &:visited': {
        '&.link, & .link': {
          color: theme('colors.primary.main'),
          '&.arrow::after, &.arrow::before': {
            backgroundColor: theme('colors.primary.main'),
          },
          '&:not(.arrow)': {
            borderColor: theme('colors.primary.main'),
          },
        },
      },
      '&:hover': {
        '&.link, & .link': {
          color: theme('colors.primary.dark'),
          '&.arrow::after, &.arrow::before': {
            backgroundColor: theme('colors.primary.dark'),
          },
          '&:not(.arrow)': {
            borderColor: theme('colors.primary.dark'),
          },
        },
      },
      '&.link': {
        padding: `4px 0`,
        lineHeight: theme('lineheight.default'),
        fontWeight: theme('fontWeight.bold'),
        display: `inline-flex`,
        justifyContent: `center`,
        alignItems: `center`,
        transitionProperty: `transform`,
        transitionTimingFunction: `cubic-bezier(0.4, 0, 0.2, 1)`,
        transitionDuration: `200ms`,
        '&.arrow': {
          '&::after': {
            display: `inline-block`,
            position: `relative`,
            width: theme('spacing.16'),
            height: theme('spacing.16'),
            backgroundColor: theme('colors.primary.main'),
            content: 'no-open-quote',
            maskImage: `url('./components/button/arrow_forward.svg')`,
            maskSize: `cover`,
          },
        },
        '&:hover': {
          '&.arrow::after': {
            animation: `translateIcon .4s infinite alternate`,
          },
        },
        '&:not(.arrow)': {
          paddingBottom: theme('spacing.0'),
          marginBottom: theme('spacing.3'),
          borderBottom: `1px solid`,
        },
        '&:focus, &:focus-visible': {
          paddingBottom: theme('spacing.4'),
          marginBottom: theme('spacing.0'),
          borderRadius: `0.125rem`,
          border: `0`,
          outline: `${theme('spacing.2')} ${theme('colors.primary.dark')} solid`,
        },
      },
    },
  });
});
